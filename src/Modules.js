import React from 'react';
import { Col, Row } from 'react-bootstrap';
import "./css/Main.css"

export class HeaderCom extends React.Component {
    constructor(props) {
        super(props);
        this.state = {src : `url(${props.src})`};
    }

    HeaderTitle = (props) => (
        <p>{props.title}</p>
    )

    render() {
        return(
            <div className="background-image" style={{backgroundImage: this.state.src}}>
                <div className="text-title">
                    <this.HeaderTitle title={this.props.title}/>
                </div>
            </div>
        )
    }
}

export const DestinationsCom = (props) => (
    <div>
        <a href="#" className="image-wrapper">
            <img src={props.src} class="zoom"></img>

            <div className="image-text">
                <h3>{props.title}</h3>
            </div>
        </a>
    </div>
)

export const ServicesCom = (props) => (
    <div className="service">
        <div className="service-icon">
            <i class={props.icon}></i>
        </div>

        <div className="service-text">
            <h4>{props.title}</h4>
            <p>{props.description}</p>
            <a href="#" className="learn-more">Learn more</a>
        </div>
    </div>
)

export const TestimonialsCom = (props) => (
    <div className="slide" style={{display:"none"}}>
        <Row> 
            <Col lg={6} md={12} className="slide-image">
                <img src={props.src}></img>
            </Col>

            <Col lg={6} md={12} className="slide-text">
                <p>{props.description}</p>
            </Col> 
        </Row>    
    </div>
)

export const Title = (props) => (
    <div className="title">
        <h1>{props.title}</h1>
    </div>
)

export const BlogCom = (props) => (
    <div className="blog-com">
        <img src={props.src} alt={props.alt} id="blogImage"></img>
        <h5 id="blogTitle">{props.blogTitle}</h5>
        <p id="blogDate">{props.blogDate}</p>
        <p id="blogText">{props.blogText}</p>
    </div>
);

export function Booking() {
    return (
        <div className="text-center booking">
            <h1>Want To Travel With Us?</h1>
            <div className="btnStyle">Booking</div>
        </div>
    )
}

export const FormButton = (props) => (
    <div id="button" className="container_row">
      <button>{props.title}</button>
    </div>
);

export const FormInput = (props) => (
    <div className="container_row form-input">
        <label>{props.description}</label>
        <input type={props.type} placeholder={props.placeholder} />
    </div>
);

export function Member(props) {
    return (
        <div>
            <img src={props.src} alt={props.alr} className="member-image"></img>
            <h3>{props.name}</h3>
            <p>Year 2 student of University of Puthisastra</p>
            <p>Majoring in ICT</p>
        </div>
    )
}


export const FormHeader = (props) => <h2 id="headerTitle">{props.title}</h2>;

export const CForm = (props) => (
    <div>
        <FormInput
            description="Last Name"
            placeholder="Enter your Last Name"
            type="text"
        />
        <FormInput
            description="First Name"
            placeholder="Enter your First Name"
            type="text"
        />
        <FormInput
            description="Email"
            placeholder="Enter your Email"
            type="email"
        />
        <FormInput
            description="Password"
            placeholder="Enter your password"
            type="password"
        />
        <FormInput
            description="Confirm Password"
            placeholder="Confirm your password"
            type="password"
        />
        <FormButton title="Sign Up"/>
    </div>
);