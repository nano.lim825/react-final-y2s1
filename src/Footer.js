import React , {Component} from "react"
import {Row, Col, Container} from 'react-bootstrap'
import 'bootstrap/dist/css/bootstrap.css'
import './css/Main.css'
import { Link } from "react-router-dom";


class Footer extends Component{
    render(){
        return (
            <div className="footer">
                <Row>
                    <Col lg={4} md={12}>
                        <h3>About Travelo</h3>
                        <p>
                            A travel agency for everyone. We provide the best and unforgettable trip for you with the affordable price. 
                            A trip that you can enjoy by yourself, with your family, friends, lover or co-workers are available for all. 
                        </p>
                    </Col>
                    <Col lg={4} md={12}>
                        <h3>Navigations</h3>
                        <div className="row footer-nav">
                            <Col lg={6} md={12}>
                                <ul>
                                    <a href=".index.html"><li>Home</li></a>
                                    <Link to="/destinations">
                                        <li>Destination</li>
                                    </Link>
                                    <a href="#"><li>Services</li></a>
                                    <Link to="/about">
                                        <li>About</li>
                                    </Link>
                                </ul>
                            </Col>

                            
                            <Col lg={6} md={12}>
                                <ul>
                                    <Link to="/discount">
                                        <li>Discount</li>
                                    </Link>
                                    <a href="#"><li>Privacy Policy</li></a>
                                    <Link to="/contact">
                                        <li>Contact</li>
                                    </Link>
                                    <a href="./sitemap.html"><li>Sitemap</li></a>
                                </ul>
                            </Col>
                        </div>
                    </Col>

                    <Col lg={4} md={12}>
                        <h3>Subscribe Newsletter</h3>
                        <p>Give us your email in order to get all the great updates and promotion from us. </p>
                        <form className="input-group">
                            <input className="form-control pl-3 bg-transparent" type="text" name="email" placeholder="Enter Email"/>
                            <input className="submit" type="submit" name="submit"/>
                        </form>
                    </Col>
                </Row>
                
                <Row>
                    <Col lg={12} md={6} className="navigation-icons">
                        <a href="#"><i className="icon fab fa-tripadvisor"></i></a>
                        <a href="#"><i className="icon fab fa-facebook-f"></i></a>
                        <a href="#"><i className="icon fab fa-twitter"></i></a>
                        <a href="#"><i className="icon fab fa-instagram"></i></a>
                    </Col>
                </Row>

            </div>
            
        );
    }
}

export default Footer;