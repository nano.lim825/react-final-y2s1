import React, { Component } from 'react'
import { FormHeader, CForm} from "./Modules"
import Footer from "./Footer"
import "./css/Main.css"

export class SignUpForm extends Component {
    render() {
        return (
        <div id="loginform">
            <FormHeader title="SIGN UP" />
            <CForm />
        </div>
        )
    }
}


class SignUp extends Component {
    render() {
        return (
            <div className="signup-page">
                <SignUpForm />
                <Footer/>
            </div>
        )
    }
}

export default SignUp
