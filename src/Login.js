import React from 'react';

import '@fortawesome/fontawesome-free/css/all.min.css';
import Footer from "./Footer"
import "./css/Main.css"

import macaron from "./img/macaron.jpg";
import camping from "./img/camping.jpg";
import dog from "./img/dog.jpg";
import beach from "./img/beach.jpg";
import coffee from "./img/coffee.jpg";

const LoginHeader = (props) => (
    <h2 className="loginheader">{props.title}</h2>
);

const FormInput = (props) => (
    <div className="content">
        <label>{props.value}</label>
        <input placeholder={props.placeholder} type={props.type}/>
    </div>
);

const FormButton = (props) => (
    <div className="content">
        <button>{props.value}</button>
    </div>
);

const Form = () => (
    <div className="container">
        <FormInput value="Username:" placeholder="Please enter username..." type="text"/>
        <FormInput value="Password:" placeholder="Please enter password..." type="password"/>
        <FormButton value="Login" />
    </div>
);

class LoginForm extends React.Component {
    render() {
        return(
            <div className="loginform">
                <LoginHeader title = "Login"/>
                <Form />
            </div>
        )
    }
}

class Login extends React.Component {
    constructor(props) {
        super(props);
        this.state = {color : `url(${macaron})`};
    }

    ChangeBackGroundColor = (color) => (
        this.setState({color})
    )

    render() {
        return(
            <div>
                <div className="login-page" id="main" style={{backgroundImage:this.state.color}} >
                    <LoginForm/>
                    <div className="color-container">

                        <div className="color-box" 
                        style={{backgroundImage:`url(${macaron})`}} 
                        onClick={() => this.ChangeBackGroundColor(`url(${macaron})`)}>
                        </div>

                        <div className="color-box" 
                        style={{backgroundImage:`url(${camping})`}} 
                        onClick={() => this.ChangeBackGroundColor(`url(${camping})`)}>
                        </div>

                        <div className="color-box" 
                        style={{backgroundImage:`url(${dog})`}} 
                        onClick={() => this.ChangeBackGroundColor(`url(${dog})`)}>
                        </div>

                        <div className="color-box" 
                        style={{backgroundImage:`url(${beach})`}} 
                        onClick={() => this.ChangeBackGroundColor(`url(${beach})`)}>
                        </div>

                        <div className="color-box" 
                        style={{backgroundImage:`url(${coffee})`}} 
                        onClick={() => this.ChangeBackGroundColor(`url(${coffee})`)}>
                        </div>
                    </div>
                </div>
                <Footer/>
            </div>      
        )
    }
}

export default Login