import React, { Component } from 'react'
import { Row, Col, Container} from 'react-bootstrap';

import { Member, Booking, ServicesCom, Title, HeaderCom } from "./Modules"
import Footer from "./Footer"


import Yonghai from "./img/yh.jpg";
import Nalis from "./img/team_member1.jpg";
import Cat from "./img/team_cat.jpg";
import blog from "./img/blog.jpg";
import about from "./img/mountain_header.jpg";

import '@fortawesome/fontawesome-free/css/all.min.css';
import "./css/Main.css"



const Header = () => (
    <div>
        <HeaderCom
            src={about}
            title="About Travelo"
        />
    </div>
)

const AboutUs = () => (
    <Container className="section">
        <Row className="my-5 pr-3">
            <Col lg={6} sm={12}>
                <img src={blog} className="mt-5"></img>
            </Col>
            
            <Col lg={6} sm={12} className="pt-5 pl-3">
                <h1 className="text-center text-justify">About Travelo</h1>
                <p className="pt-3">
                    A travel agency for everyone. We provide the best and unforgettable trip for you with the affordable price. 
                    A trip that you can enjoy by yourself, with your family, friends, lover or co-workers are available for all. 
                </p>


                <ul>
                    <Row className="list">
                        <svg width="2em" height="2em" viewBox="0 0 16 16" className="bi bi-check" fill="#ff6600" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z"/>
                        </svg>
                        <li>Lorem ipsum dolor sit amet</li>
                    </Row>

                    <Row className="list">
                        <svg width="2em" height="2em" viewBox="0 0 16 16" className="bi bi-check" fill="#ff6600" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z"/>
                        </svg>
                        <li>Lorem ipsum dolor sit amet</li>
                    </Row>

                    <Row className="list">
                        <svg width="2em" height="2em" viewBox="0 0 16 16" className="bi bi-check" fill="#ff6600" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M10.97 4.97a.75.75 0 0 1 1.071 1.05l-3.992 4.99a.75.75 0 0 1-1.08.02L4.324 8.384a.75.75 0 1 1 1.06-1.06l2.094 2.093 3.473-4.425a.236.236 0 0 1 .02-.022z"/>
                        </svg>
                        <li>Lorem ipsum dolor sit amet</li>
                    </Row>
                </ul>
            </Col>
            
        </Row>
    </Container>
)

const Members = () => (
    <Container className="team-members section">
        <div><h1 className="text-center primary-color">Our Team</h1></div>
        <Row className="members">
            <Col lg={4} md={6} className="member-desc text-center">
                <Member 
                    src={Yonghai}
                    name="Yonghai LIM" 
                />

                <Row>
                    <div className="navigation-icons col-lg-12 col-md-6">
                        <a href="#"><i className="icon fab fa-facebook-f" id="icon-facebook"></i></a>
                        <a href="#"><i className="icon fab fa-twitter" id="icon-twitter"></i></a>
                        <a href="#"><i className="icon fab fa-instagram" id="icon-ig"></i></a>
                    </div>
                </Row>
            </Col>


            <Col lg={4} md={6} className="member-desc">
                <img src={Cat} className="member-image"></img>
                <h2>Team Travelo</h2>
            </Col>

            <Col lg={4} md={6} className="member-desc text-center">
                <Member
                    src={Nalis}
                    name="Nalis UN" 
                />

                <Row>
                    <div className="navigation-icons col-lg-12 col-md-6">
                        <a href="#"><i className="icon fab fa-facebook-f" id="icon-facebook"></i></a>
                        <a href="#"><i className="icon fab fa-twitter" id="icon-twitter"></i></a>
                        <a href="#"><i className="icon fab fa-instagram" id="icon-ig"></i></a>
                    </div>
                </Row>
            </Col>
        </Row>
    </Container>
)

const Services = () => (
    <Container className="section">
        <Title title="Services"/>
        <Row>
            <Col lg={4} md={6} sm={12}>
                <ServicesCom 
                    icon="fas fa-plane"
                    title="Air Ticket" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
            <Col lg={4} md={6} sm={12}>
                <ServicesCom 
                    icon="fas fa-ship"
                    title="Cruises" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
            <Col lg={4} md={6} sm={12}>
                <ServicesCom 
                    icon="fas fa-search-location"
                    title="Tour Packages" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
            <Col lg={4} md={6} sm={12}>
                <ServicesCom 
                    icon="fas fa-hotel"
                    title="Hotel" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
            <Col lg={4} md={6} sm={12}>
                <ServicesCom 
                    icon="fas fa-water"
                    title="Sea Explorations" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
            <Col lg={4} md={6} sm={12}>
                <ServicesCom 
                    icon="fas fa-skiing"
                    title="Ski Experiences" 
                    description="
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit.
                    Saepe pariatur reprehenderit vero atque, consequatur id ratione"
                />
            </Col>
        </Row>
    </Container>
)

class About extends React.Component {
    render() {
        return (
            <div className="about-page">
                <Header/>
                <AboutUs/>
                <Members/>
                <Services/>                 
                <Booking/>
                <Footer/>        
            </div>
        );
    }
}

export default About;
